import { Action, createSelector } from '@ngrx/store';
import { VideosActions, VideosActionTypes } from '../actions/videos.actions';
import { User, Video } from 'src/app/models';

export interface State {
  credits: string;
  user: User | null;
  videoQuery: string;
  videos: Video[];
  selectedVideoId: string;
}

export const initialState: State = {
  credits: 'Szkolenie Angular 2018',
  user: null,
  videoQuery: '',
  videos: [],
  selectedVideoId: null
};

export function reducer(state = initialState, action: VideosActions): State {

  switch (action.type) {

    case VideosActionTypes.UserLoginResponse:
      return {
        ...state,
        user: action.payload
      };

    case VideosActionTypes.UpdateCredits:
      return {
        ...state,
        credits: action.payload
      };

    case VideosActionTypes.VideosSearch:
      return {
        ...state,
        videoQuery: action.payload
      };

    case VideosActionTypes.VideosSearchResponse:
      return {
        ...state,
        videos: action.payload
      };

    case VideosActionTypes.VideoSelect:
      return {
        ...state,
        selectedVideoId: action.payload
      };


    default:
      return state;
  }
}

export const getVideos = (state: State) => state.videos;
export const getSelectedVideoId = (state: State) => state.selectedVideoId;

export const getSelectedVideo = createSelector(getVideos, getSelectedVideoId, (videos, selectedId) => {
  return videos.find(video => video.id.videoId === selectedId);
});
