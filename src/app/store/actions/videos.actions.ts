import { Action } from '@ngrx/store';
import { User, Video } from 'src/app/models';

export enum VideosActionTypes {
  LoadVideoss = '[Videos] Load Videoss',
  UpdateCredits = '[Videos] Update Credits',

  UserLogin = '[User] User Login',
  UserLoginResponse = '[User] User Login Response',

  VideosSearch = '[Videos] Videos Search',
  VideosSearchResponse = '[Videos] Videos Search Response',
  VideosSearchError = '[Videos] Videos Search Error',

  VideoSelect = '[Videos] Video Select',
}

export class LoadVideoss implements Action {
  readonly type = VideosActionTypes.LoadVideoss;
}

export class UpdateCreditsAction implements Action {
  readonly type = VideosActionTypes.UpdateCredits;
  constructor(public payload: string) { }
}

export class UserLoginAction implements Action {
  readonly type = VideosActionTypes.UserLogin;
}

export class UserLoginResponseAction implements Action {
  readonly type = VideosActionTypes.UserLoginResponse;
  constructor(public payload: User | null) { }
}

export class VideosSearchAction implements Action {
  readonly type = VideosActionTypes.VideosSearch;
  constructor(public payload: string) { }
}

export class VideoSearchResponseAction implements Action {
  readonly type = VideosActionTypes.VideosSearchResponse;
  constructor(public payload: Video[]) { }
}

export class VideoSearchErrorAction implements Action {
  readonly type = VideosActionTypes.VideosSearchResponse;
  constructor(public payload: any) { }
}

export class VideoSelectAction implements Action {
  readonly type = VideosActionTypes.VideoSelect;
  constructor(public payload: string) { }
}

export type VideosActions =
  LoadVideoss |
  UpdateCreditsAction |
  UserLoginResponseAction |
  UserLoginAction |
  VideosSearchAction |
  VideoSearchResponseAction |
  VideoSelectAction |
  VideoSearchErrorAction;
