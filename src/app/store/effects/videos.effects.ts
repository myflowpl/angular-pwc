import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { VideosActionTypes, UserLoginResponseAction, VideoSearchResponseAction } from '../actions/videos.actions';
import { switchMap, map } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/services/auth.service';
import { SearchService } from 'src/app/videos/search.service';

@Injectable()
export class VideosEffects {

  @Effect()
  userLogin$ = this.actions$.pipe(
    ofType(VideosActionTypes.UserLogin),
    switchMap(() => {
      return this.authService.loginDialog$.pipe(
        map(user => new UserLoginResponseAction(user))
      );
    })
  );

  @Effect()
  videosSearch$ = this.actions$.pipe(
    ofType(VideosActionTypes.VideosSearch),

    switchMap((action: any) => {
      return this.searchService.searchYoutube(action.payload).pipe(
        map(videos => new VideoSearchResponseAction(videos))
      );
    })
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private searchService: SearchService
  ) { }
}
